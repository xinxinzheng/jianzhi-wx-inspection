import { createAction } from 'redux-actions'
import * as userTypes from '../types/user'
import * as userService from '../../services/user'
import user from "../reducers/user";

export const fetchUserToken = createAction(userTypes.FETCH_USER_TOKEN, data => {
  return userService.fetchUserToken(data)
})

export const bindUser = createAction(userTypes.BIND_USER, data => {
  return userService.bindUser(data)
})

export const sign = createAction(userTypes.SIGN, data => {
  return userService.sign(data)
})

export const createReport = createAction(userTypes.CREATE_REPORT, data => {
  return userService.createReport(data)
})

export const updateUser = createAction(userTypes.UPDATE_USER, data => {
  return userService.updateUser(data)
})

export const fetchUserInfo = createAction(userTypes.FETCH_USER_INFO, data => {
  return userService.fetchUserInfo(data)
})

export const getNote = createAction(userTypes.GET_NOTE, data => {
  return userService.getNote(data)
})

export const getUserAuth = createAction(userTypes.GET_USER_AUTH, (data) => {
  return userService.getUserAuth(data)
})

export const getUserTask = createAction(userTypes.GET_USER_TASK, (data) => {
  return userService.getUserTask(data)
})

export const getCompany = createAction(userTypes.GET_COMPANY, (data) => {
  return userService.getCompany(data)
})

export const getUserList = createAction(userTypes.GET_USER_LIST, (data) => {
  return userService.getUserList(data)
})

export const getDomain = createAction(userTypes.GET_DOMAIN, (data) => {
  return userService.getDomain(data)
})


