import { handleActions } from 'redux-actions'
import * as userTypes from '../types/user'

export default handleActions({
  [userTypes.FETCH_USER_TOKEN](state, { payload }) {
    return {
      ...state,
      uid: payload
    }
  },[userTypes.FETCH_USER_INFO](state, { payload }) {
    return {
      ...state,
      userInfo: payload
    }
  },
  [userTypes.GET_USER_LIST](state, { payload }) {
    return {
      ...state,
      userlist: payload
    }
  },[userTypes.GET_COMPANY](state, { payload }) {
    return {
      ...state,
      companylist: payload
    }
  },[userTypes.GET_DOMAIN](state, { payload }) {
    return {
      ...state,
      domainlist: payload
    }
  },[userTypes.GET_NOTE](state, { payload }) {
    return {
      ...state,
      notelist: payload
    }
  },[userTypes.GET_USER_TASK](state, { payload }) {
    return {
      ...state,
      tasklist: payload
    }
  },[userTypes.BIND_USER](state, { payload }) {
    return {
      ...state
    }
  },[userTypes.SIGN](state, { payload }) {
    return {
      ...state
    }
  },[userTypes.UPDATE_USER](state, { payload }) {
    return {
      ...state
    }
  }
}, {
  uid: null,
  userAuth:{},
  userSomeAuth:{}
})
