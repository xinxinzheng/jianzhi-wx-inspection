export const grouped4row = (array, countsPerRow) => {
  const src = [...array]
  const dest = []
  while (src.length > 0) {
    dest.push(src.splice(0, countsPerRow))
  }
  return dest
}

export const json2Form = (json) => {
  const str = []
  for (const p in json) {
    str.push(`${encodeURIComponent(p)}=${encodeURIComponent(json[p])}`)
  }
  return str.join('&')
}

export const isDate = (date) => {
  // 是否为Date对象
  return date && date.getMonth
}

export const isLeapYear = (year) => {
  // 传入为时间格式需要处理 是否为闰年
  if (isDate(year)) year = year.getFullYear()
  if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) return true
  return false
}

export const getDaysOfMonth = (date) => {
  // 获取月份所有天数
  var month = date.getMonth() // 注意此处月份要加1，所以我们要减一
  var year = date.getFullYear()
  return [
      31,
      isLeapYear(year) ? 29 : 28,
      31,
      30,
      31,
      30,
      31,
      31,
      30,
      31,
      30,
      31
  ][month]
}

export const getBeginDayOfMouth = (date) => {
  // 获取当前月份第一天 星期
  var month = date.getMonth()
  var year = date.getFullYear()
  var d = new Date(year, month, 1)
  return d.getDay()
}
export const formatNum = (n) => {
  if (n < 10) return '0' + n
  return n.toString()
}
export const getDayName = (dayMap, year, month, day) => {
  var name = formatNum(year) + formatNum(parseInt(month) + 1) + formatNum(day)
  return {
    sat:  dayMap.sat.filter((item) => { return item.time === name}).length ? true : false,
    act:  dayMap.act.filter((item) => { return item.time === name}).length ? true : false,
    tofel:  dayMap.tofel.filter((item) => { return item.time === name}).length ? true : false
  }
}
export const getId = (dayMap, year, month, day, type) => {
  var name = formatNum(year) + formatNum(parseInt(month) + 1) + formatNum(day)
  let ids = {
    sat:  dayMap.sat.filter((item) => { return item.time === name}).length && dayMap.sat.filter((item) => { return item.time === name})[0].id,
    act:  dayMap.act.filter((item) => { return item.time === name}).length && dayMap.act.filter((item) => { return item.time === name})[0].id,
    tofel:  dayMap.tofel.filter((item) => { return item.time === name}).length && dayMap.tofel.filter((item) => { return item.time === name})[0].id
  }
  return ids[type.toLowerCase()]
}
export const isSelected = (date, year, month, day) => {
  if (
    date.getFullYear() === year &&
    date.getMonth() === month &&
    date.getDate() === day
  ) {
    return true
  }
  return false
}