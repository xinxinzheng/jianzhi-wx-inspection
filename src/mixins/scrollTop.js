import wepy from 'wepy'

export default class ScrollTopMixin extends wepy.mixin {
  data = {
    scrollTop: 0,
    maxScrollOffsetY: 440
  }

  onPageScroll({ scrollTop }) {
    this.scrollTop = scrollTop
    if (scrollTop > this.maxScrollOffsetY) {
      return
    }
    this.$apply()
  }
}
