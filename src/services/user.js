import wepy from 'wepy'

export const fetchUserToken = data => wepy.request({
  url: '/wxa/getWxaOpenid',
  method: 'GET',
  data
})

export const fetchUserInfo = data => wepy.request(`/user/detail?uid=${wepy.getStorageSync('uid')}`)

export const getUserAuth = (data) => wepy.request({
  url: '/permission/list',
  method: 'GET',
  data: {
    uid: wepy.getStorageSync('uid'),
    ...data
  }
})

export const getUserTask = (data) => wepy.request({
  url: '/xjrw/wxaqueryXjrw',
  method: 'GET',
  data: {
    ...data
  }
})

export const getUserList = (data) => wepy.request({
  url: '/xjy/getXjy',
  method: 'GET',
  data: {
    ...data
  }
})

export const getDomain = (data) => wepy.request({
  url: '/dic/getDic?dicBh=XCFW',
  method: 'GET',
  data: {
    ...data
  }
})

export const getCompany = (data) => wepy.request({
  url: '/dic/getDic?dicBh=XCDW',
  method: 'GET',
  data: {
    ...data
  }
})

export const getNote = (data) => wepy.request({
  url: '/gg/queryGg',
  method: 'GET',
  data: {
    ...data
  }
})

export const bindUser = (data) => wepy.request({
  url: '/wxa/bindUserWithOpenid',
  method: 'GET',
  data: {
    ...data
  }
})

export const sign = (data) => wepy.request({
  url: '/qd/qiandao',
  method: 'GET',
  data: {
    ...data
  }
})

export const updateUser = (data) => wepy.request({
  url: '/xjy/editXjy',
  method: 'GET',
  data: {
    ...data
  }
})


export const createReport = (data) => wepy.request({
  url: '/jcbg/addJcbg',
  method: 'POST',
  header: { "Content-Type": "multipart/form-data"},
  data: {
    ...data
  }
})